<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('subcategory_id')->nullable();

            $table->string('title')->nullable();
            $table->longText('description')->nullable();
            $table->binary('image')->nullable();
            $table->text('image_meta_data')->nullable();
            $table->decimal('price', 8,2)->nullable();
            $table->integer('quantity')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('category_id')->on('categories')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->foreign('subcategory_id')->on('sub_categories')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
