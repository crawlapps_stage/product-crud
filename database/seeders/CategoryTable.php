<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\SubCategory;

class CategoryTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'title' => 'Clothes',
                'subcategory' => [
                    'Shirts',
                    'T-Shirts',
                    'Tops',
                    'Jeans'
                ]
            ],
            [
                'title' => 'Vehicles' ,
                'subcategory' => [
                    'Cars',
                    'Motorcycles',
                    'Trucks',
                ]
            ]
        ];

        foreach($categories as $key=>$category){
            $db_category = new Category();
            $db_category->title = $category['title'];
            $db_category->save();

            foreach($category['subcategory'] as $subcategory){
                $db_subcategory = new SubCategory();
                $db_subcategory->category_id = $db_category->id;
                $db_subcategory->title = $subcategory;
                $db_subcategory->save();
            }
        }
    }
}
