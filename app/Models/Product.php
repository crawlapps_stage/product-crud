<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    public function Categories()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
    public function SubCategories()
    {
        return $this->belongsTo(SubCategory::class, 'subcategory_id', 'id');
    }
}
