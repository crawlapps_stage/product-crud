<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $products = Product::with('Categories', 'SubCategories')->get();
            $data['products'] = ProductResource::collection($products);
            return response()->json(['data' => $data], 200);
        }catch(\Exception $e){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $data = json_decode($request->data, true);
            $file = $request->image;

            $contents = (gettype($file) == 'string') ? $file : $file->openFile()->fread($file->getSize());

            $db_product = ($data['id']) ? Product::find($data['id']) : new Product;
            $db_product->category_id = $data['category_id'];
            $db_product->subcategory_id = $data['subcategory_id'];
            $db_product->title = $data['title'];
            $db_product->description = $data['description'];

            if((gettype($file) != 'string')){
                $db_product->image = $contents;
            }

            $db_product->image_meta_data = $data['image_meta_data'];
            $db_product->price = $data['price'];
            $db_product->quantity = $data['quantity'];
            $db_product->save();

            $action = ($data['id']) ? 'updated' : 'created';
            return response()->json(['data' => "Product $action successfully"], 200);
        }catch(\Exception $e){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id = '')
    {
        try{
            $data['product'] = [];
            if($id != ''){
                $product = Product::find($id);
                $data['product'] = $product;
                $data['product']['image'] = base64_encode($data['product']['image']);
            }

            $category = Category::select('id', 'title')->with('SubCategories')->get();

            $data['category'] = $category;

            return response()->json(['data' => $data], 200);
        }catch(\Exception $e){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $product = Product::find($id);
            $product->delete();
            return response()->json(['data' => 'Product Deleted'], 200);
        }catch(\Exception $e){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }
}
