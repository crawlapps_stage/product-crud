<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'category' => $this->categories->title,
            'subcategory' => $this->SubCategories->title,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'image_meta_data' => $this->image_meta_data,
            'image' => base64_encode($this->image),
        ];
    }
}
