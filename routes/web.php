<?php

use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/product.index', [ProductController::class,'index'])->name('product.index');
Route::get('/product.edit/{id?}', [ProductController::class,'edit'])->name('product.edit');
Route::post('/product.store', [ProductController::class,'store'])->name('product.store');
Route::delete('/product.delete/{id?}',  [ProductController::class,'destroy'])->name('product.delete');

Route::get('/order', function () {
    return view('order');
});
Route::get('/order.index', [OrderController::class,'index'])->name('order.index');
Route::get('/order.fetchproduct/{category_id}/{subcategory_id}', [OrderController::class,'fetchProduct'])->name('order.fetchproduct');
Route::post('/order.store', [OrderController::class,'store'])->name('order.store');


