import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

const routes = [
    {
        path:'/',
        component: require('../components/pages/products/index.vue').default,
        name:'products',
        meta: {
            title: 'Products',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/create-product',
        component: require('../components/pages/products/CreateProduct.vue').default,
        name:'create-product',
        meta: {
            title: 'Create Product',
            ignoreInMenu: 1,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    // {
    //     path:'/',
    //     component: require('../components/pages/products/index.vue').default,
    //     name:'charts',
    //     meta: {
    //         title: 'Charts',
    //         ignoreInMenu: 0,
    //         displayRight: 0,
    //         dafaultActiveClass: '',
    //     },
    // },
];
// This callback runs before every route change, including on page load.
const router = new VueRouter({
    mode:'history',
    routes,
    scrollBehavior() {
        return {
            x: 0,
            y: 0,
        };
    },

});

export default router;
