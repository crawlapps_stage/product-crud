require('./bootstrap');

window.Vue = require('vue').default;

import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
Vue.use(VueToast, {
    // One of the options
    position: 'top-right',
});

import App from './components/front/order.vue';

import router from './routes';

const app = new Vue({
    el: '#front',
    router,
    render: h => h(App),
});
