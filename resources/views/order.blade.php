<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" value="{{ csrf_token() }}" />

    <title>Order</title>

    <!-- App CSS -->
    <link id="theme-style" rel="stylesheet" href="/admin_theme/assets/css/portal.css">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>

<body>
    <div id="front"></div>
    <script src="{{ asset('js/front.js') }}"></script>
</body>

</html>
